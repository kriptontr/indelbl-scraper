const express = require('express')

const app = express()
const port = 3000

const puppeteer = require('puppeteer');

let browser
const singlefile = require("fs").readFileSync("./singlefile.js",{encoding:"utf-8"})
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.post('/', async (req, res) => {
    console.log(req.body);
    const page = await browser.newPage();
    await page.setBypassCSP(true)
      page.goto(req.body.url);
    await page.waitForNavigation({waitUntil: 'networkidle0'});
    await page.addScriptTag({content:  singlefile })

    const data =  await page.evaluate(()=> {
       console.log("magic starts")
       return new Promise(((resolve, reject) => {
           setTimeout(()=>{
               singlefile.getPageData().then(resolve)
           } ,1000)
       }))


    })

    var SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    while (SCRIPT_REGEX.test(data.content)) {
        data.content = data.content.replace(SCRIPT_REGEX, "");
    }

    res.json(data)

    require("fs").writeFileSync("./data/"+Date.now()+".html",data.content)
        await page.close();

})



puppeteer.launch({    args: ['--no-sandbox'], headless:false})
        .then(a => {
            browser = a ;

            app.listen(port, () => {
                console.log(`Example app listening at http://localhost:${port}`)
            })
        })






process.on("SIGTERM", () => {
    browser.close()
        .then(() => {
            process.exit(0)
        })

})
